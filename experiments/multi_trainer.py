from torch import nn
from transformers import Trainer

""" Adapted from MultiEURLEX by Chalkidis et al. (2021) [1,2]  
    [1]: https://doi.org/10.48550/arXiv.2109.00904 
    [2]: https://github.com/nlpaueb/multi-eurlex """


class MultilabelTrainer(Trainer):
    def compute_loss(self, model, inputs, return_outputs=False):
        labels = inputs.pop("labels")
        outputs = model(**inputs)
        logits = outputs.logits
        loss_fct = nn.BCEWithLogitsLoss()
        loss = loss_fct(logits.view(-1, self.model.config.num_labels),
                        labels.float().view(-1, self.model.config.num_labels))
        return (loss, outputs) if return_outputs else loss
