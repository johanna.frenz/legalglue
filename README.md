#LegalGLUE

This is the code used for the experiments described in the bachelor thesis "LegalGLUE: Benchmarking
Legal Natural Language Processing Models".

More information can be found at https://huggingface.co/datasets/jfrenz/legalglue
